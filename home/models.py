from __future__ import unicode_literals
from django.db import models
from wagtail.search import index
from wagtail.core import blocks
from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import (
FieldPanel, StreamFieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel, PageChooserPanel)
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from modelcluster.fields import ParentalKey
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtailmedia.blocks import AbstractMediaChooserBlock
from django.utils.html import format_html
from wagtail.embeds.blocks import EmbedBlock

class HomePage(Page):
    body = RichTextField(blank=True)
    videoName = models.CharField(max_length=250, default="videoplayback")

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('videoName', classname="full"),
    ]
class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]
class BlogPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        FieldPanel('body', classname="full"),
    ]
class GalleryPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = StreamField([
        ('image', ImageChooserBlock()),
    ], null=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        StreamFieldPanel('body', classname="full"),
    ]
class InfoPage(Page):
    body = StreamField([
        ('image', ImageChooserBlock()),
        ('text', blocks.RichTextBlock()),
        ('email', blocks.EmailBlock()),
        ('subject', blocks.CharBlock()),
    ])

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]
class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE, related_name='form_fields')


class FormPage(AbstractEmailForm):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)
    form_page_landing = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

def render_landing_page(self, request, form_submission=None, *args, **kwargs):
    if self.form_page_landing:
        url = self.form_page_landing.url
        # if a form_submission instance is available, append the id to URL
        # when previewing landing page, there will not be a form_submission instance
        if form_submission:
             url += '?id=%s' % form_submission.id
        return redirect(url, permanent=False)
    # if no thank_you_page is set, render default landing page
    return super().render_landing_page(request, form_submission, *args, **kwargs)
    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('thank_you_text', classname="full"),
        PageChooserPanel('form_page_landing'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]
class FormPageLanding(Page):
    body = StreamField([
        ('image', ImageChooserBlock()),
        ('text', blocks.RichTextBlock()),
    ])

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]
